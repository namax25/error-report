<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Log\Formatter;

class ColorCli extends \Zend\Log\Formatter\Base
{

    /**
     * Formats data into a single line to be written by the writer.
     *
     * @param array $event event data
     * @return string formatted line to write to the log
     */
    public function format($event)
    {
        if (isset($event['extra']) && $event['extra']) {
            $extraValue = $this->normalize($event['extra']);
        } else {
            $extraValue = '';
        }
        $output = '';

        $output .= $this->setColorFromPriority($event['priority']) . " " . $event['message'];
        if ($extraValue) {
            $output .= "\n" . \Colors::getString('[+] ' . $extraValue, 'dark_gray');
        }
        return $output;
    }

    /**
     * {@inheritDoc}
     */
    public function getDateTimeFormat()
    {
        return $this->dateTimeFormat;
    }

    /**
     * {@inheritDoc}
     */
    public function setDateTimeFormat($dateTimeFormat)
    {
        $this->dateTimeFormat = (string)$dateTimeFormat;
        return $this;
    }

    public function setColorFromPriority($type)
    {
        $result = "[+]";
        switch ($type) {
            case \Log\Logger::EMERG:
                $result = "\n\n" . \Colors::getString('---------- [EMERGENCY] ----------', 'black', 'red');
                break;
            case \Log\Logger::ALERT:
                $result = \Colors::getString('[ALERT]', 'yellow', 'red');
                break;
            case \Log\Logger::CRIT:
                $result = \Colors::getString('[CRITICAL]', 'white', 'red');
                break;
            case \Log\Logger::ERR:
                $result = \Colors::getString('[ERROR]', 'red');
                break;
            case \Log\Logger::WARN:
                $result = \Colors::getString('[WARN]', 'yellow');
                break;
            case \Log\Logger::NOTICE:
                $result = \Colors::getString('[NOTICE]', 'black', 'yellow');
                break;
            case \Log\Logger::INFO:
                $result = \Colors::getString('[INFO]', 'blue');
                break;
            case \Log\Logger::DEBUG:
                $result = \Colors::getString('[DEBUG]', 'dark_gray');
                break;
            case \Log\Logger::OK:
                $result = \Colors::getString('[OK]', 'light_green');
                break;
            case \Log\Logger::DONE:
                $result = \Colors::getString('[DONE]', 'purple');
                break;
            case \Log\Logger::START:
                $result = "\n" . \Colors::getString('----- [START]', 'light_cyan');
                break;
            case \Log\Logger::END:
                $result = \Colors::getString('----- [END]', 'light_cyan');
                break;
            case \Log\Logger::STATUS:
                $result = \Colors::getString('[STATUS]', 'cyan');
                break;
        }
        return $result;
    }

}
