<?php

namespace Db\Dao\ErrorReportDb;

/**
 *
 * @author namax
 */
class Jobs extends \Db\Dao\DaoAbstract
{

    protected $table = "jobs";

    public function __construct(\Zend\Db\Adapter\Adapter $db)
    {
        parent::__construct($db);
    }

    public function getByName($name)
    {
        $name = \Helpers\Strings::create()->clearAlias($name);
        $result = $this->getTableGateway()->select(['name' => $name]);
        return $result->current();
    }
}
