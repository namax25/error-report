<?php
namespace Db\Dao;

/**
 * Description of Abstarct
 * Common methods
 * @author namax
 */
abstract class DaoAbstract
{

    /**
     * Db adapter
     * @var \Zend\Db\Adapter\Adapter
     */
    public $db = null;
    protected $table = null;
    protected $tableGateway = null;

    public function __construct(\Zend\Db\Adapter\Adapter $db)
    {
        $this->db = $db;
        $this->tableGateway = new \Zend\Db\TableGateway\TableGateway($this->table, $db);
    }

    public function getById($id)
    {
        $id = \Helpers\Strings::create()->clearDigits($id);
        $result = $this->getDb()->query('SELECT * FROM ' . $this->table . ' WHERE `id` = ? ', [$id]);
        return $result->current();
    }

    public function getAll()
    {
        $result = $this->getTableGateway()->select();
        return $result;
    }

    public function getOne($order = 'id DESC')
    {
        $result = $this->getTableGateway()->select(function (\Zend\Db\Sql\Select $select) use ($order) {
            $select->order($order)->limit(1);
        });
        return $result->current();
    }

    public function insert($data)
    {
        return $this->tableGateway->insert($data);
    }

    public function update($data, $where)
    {
        return $this->tableGateway->update($data, $where);
    }

    public function delete($where)
    {
        return $this->tableGateway->delete($where);
    }

    /**
     *
     * @return \Zend\Db\Adapter\Adapter
     */
    public function getDb()
    {
        return $this->db;
    }

    /**
     *
     * @return  \Zend\Db\TableGateway\AbstractTableGateway
     */
    public function getTableGateway()
    {
        return $this->tableGateway;
    }

    public function setDb(\Zend\Db\Adapter\Adapter $db)
    {
        $this->db = $db;
    }

    public function setTableGateway($tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
}
