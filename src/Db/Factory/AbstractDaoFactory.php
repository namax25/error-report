<?php

namespace Db\Factory;

/**
 * Date: 4/28/15
 * Time: 5:55 AM
 * @author namax
 */
abstract class AbstractDaoFactory
{

    /**
     *
     * @var \Zend\Db\Adapter\Adapter
     */
    protected $db = null;

    private $instancesCache = [];

    public function __construct(\Zend\Db\Adapter\Adapter $db)
    {
        $this->setDb($db);
    }

    /**
     *
     * @return \Zend\Db\Adapter\Adapter
     */
    public function getDb()
    {
        return $this->db;
    }

    public function setDb(\Zend\Db\Adapter\Adapter $db)
    {
        $this->db = $db;
    }

    protected function getDaoClass($className, $newDao = false)
    {
        if ($this->isInstanceNotExists($className) || $newDao === true) {
            $this->instancesCache[$className] = new $className($this->getDb());
        }
        return $this->instancesCache[$className];
    }

    private function isInstanceNotExists($className)
    {
        return empty($this->instancesCache[$className]);
    }

    public function clearCache()
    {
        $this->instancesCache = [];
    }
}


