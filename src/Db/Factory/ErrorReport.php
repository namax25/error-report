<?php

namespace Db\Factory;
use Db\Dao\ErrorReportDb;

/**
 * Description of DaoFactory
 *
 * @author namax
 */
class ErrorReport extends AbstractDaoFactory
{
    /**
     * @param bool $newDao
     * @return ErrorReportDb\Jobs
     */
    public function getJobs($newDao = false)
    {
        return $this->getDaoClass(ErrorReportDb\Jobs::class, $newDao);
    }

    /**
     * @param bool $newDao
     * @return ErrorReportDb\RawEmails
     */
    public function getRawEmails($newDao = false)
    {
        return $this->getDaoClass(ErrorReportDb\RawEmails::class, $newDao);
    }
}
