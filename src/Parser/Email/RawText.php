<?php
/**
 * Created by IntelliJ IDEA.
 * User: namax
 * Date: 22/08/16
 * Time: 04:45 PM
 */

namespace Parser\Email;

class RawText implements EmailParserInterface
{
    /**
     * @param $dataForParse
     * @param $out
     * @return mixed|void
     */
    public function parse($dataForParse, &$out)
    {
        $cleanParts = $this->clean($dataForParse);
        $errors = $this->parseErrors($cleanParts);
        return $errors;
    }

    private function parseErrors($cleanParts)
    {

        $title = $cleanParts[0];
        $titleParts = explode("Error summary report", $title);
        $serverUrl = null;
        $errorDate = null;

        if (count($titleParts) == 2) {
            $serverUrl = trim($titleParts[0]);
            $reportTimeStr = trim($titleParts[1]);
            $errorDate = $this->tryToParseDateString($reportTimeStr);
        }

        $errors = [];
        foreach ($cleanParts as $cleanPart) {
            $matches = [];
            preg_match("/(\d+)x:\s(.+)\((\d+)\)\s(.+)/", $cleanPart, $matches);
            if (count($matches) == 5) {
                $error['count_error_occured'] = $matches[1];
                $error['file_path'] = $matches[2];
                $error['line'] = $matches[3];
                $error['error_text'] = $matches[4];
                $error['error_date'] = $errorDate;
                $error['server_url'] = $serverUrl;
                $errors[] = $error;
            }
        }

        return $errors;
    }

    private function clean($dataForParse)
    {

        $dataForParse = preg_replace("/(=\n0?A?)+/", "", $dataForParse);
        $emailParts = explode("\n", $dataForParse);

        $cleanParts = [];
        foreach ($emailParts as $part) {
            $cleanPart = trim($part);
            $cleanParts[] = $cleanPart;
        }
        return $cleanParts;
    }

    private function tryToParseDateString($dateStr)
    {
        try {
            return new \DateTime($dateStr);
        } catch (\Exception $e) {
            return null;
        }
    }

}