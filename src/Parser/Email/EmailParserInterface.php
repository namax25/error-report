<?php
/**
 * Created by IntelliJ IDEA.
 * User: namax
 * Date: 22/08/16
 * Time: 04:51 PM
 */

namespace Parser\Email;

interface EmailParserInterface
{

    /**
     * @param $dataForParse
     * @param $out
     * @return mixed
     * @internal param $data
     */
    public function parse($dataForParse, &$out);

}