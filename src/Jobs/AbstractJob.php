<?php

namespace Jobs;

/**
 * Description of AbstractGrabbers
 *
 * @author namax
 */
abstract class AbstractJob
{

    /**
     * Zend Service manager
     * @var
     */
    protected $serviceManager;

    /**
     * Console log
     * @var
     */
    protected $logCli;

    /**
     * Db lob
     * @var
     */
    protected $logDb;

    /**
     * @var
     */
    protected $daoGrabbers;

    /**
     * @var
     */
    protected $daoJobs;

    /**
     * Job's name
     * @var
     */
    protected $jobName;

    /**
     * @var
     */
    protected $daoFactoryErrorReport;


    /**
     * Page encoding
     * @var type
     */
    protected $encoding = null;

    /**
     *
     */
    protected $jobRawData;

    /**
     *
     * @var \Zend\Config\Config
     */
    protected $settings;

    /**
     * @var null
     */
    protected $idStatic = null;

    final public function __construct(\Zend\ServiceManager\ServiceLocatorInterface $serviceManager, $jobName)
    {
        $this->setServiceManager($serviceManager);
        $this->jobName = $jobName;

        //should be in the contructor to have opportunity to get params in the run files
        $this->initSettings();
    }

    public function run()
    {
        try {
            $this->updateJobStartDate($this->getJobRawData()->id);
            $startTime = microtime(true);
            $this->configure();
            $this->doAll();
            $endTime = round((microtime(true) - $startTime), 4);
            $this->getLogCli()->log(\Log\Logger::DONE, "SUCCESS. Elapsed time: " . $endTime);
            $this->updateJobElapsedTime($this->getJobRawData()->id, $endTime);
        } catch (\Exception $ex) {
            $this->getLogCli()->err($ex);
            $this->getLogDb()->err($ex,
                ['id' => $this->getJobRawData()->id, "job_name" => $this->getJobRawData()->name]);
        }
    }

    /**
     *
     * @return \Zend\Config\Config
     */
    public function getSettings()
    {
        return $this->settings;
    }

    public function setSettings(\Zend\Config\Config $settings)
    {
        $this->settings = $settings;
    }

    private function initSettings()
    {
        $this->jobRawData = $this->getDaoJobs()->getByName($this->jobName);
        if (!$this->jobRawData['published']) {
            throw new \Exception(__METHOD__ . " Job is off. Published = 0. Job name is " . $this->jobName);
        }
        $jsonReader = new \Zend\Config\Reader\Json();
        $this->settings = new \Zend\Config\Config($jsonReader->fromString($this->jobRawData['params']));
    }

    protected abstract function configure();

    protected abstract function doAll();

    protected function getServiceManager()
    {
        return $this->serviceManager;
    }

    protected function setServiceManager(\Zend\ServiceManager\ServiceLocatorInterface $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    /**
     *
     * @return \Zend\Log\LoggerInterface
     */
    protected function getLogCli()
    {
        if ($this->logCli === null) {
            return $this->getServiceManager()->get('log_cli');
        }
        return $this->logCli;
    }

    /**
     *
     * @return \Zend\Log\LoggerInterface
     */
    protected function getLogDb()
    {
        if ($this->logDb === null) {
            return $this->getServiceManager()->get('log_db');
        }
        return $this->logDb;
    }

    protected function setLogCli(\Zend\Log\LoggerInterface $logCli)
    {
        $this->logCli = $logCli;
    }

    protected function setLogDb(\Zend\Log\LoggerInterface $logDb)
    {
        $this->logDb = $logDb;
    }


    /**
     * @return \Db\Dao\ErrorReport\Jobs
     */
    protected function getDaoJobs()
    {
        if ($this->daoJobs === null) {
            $this->daoJobs = $this->getDaoFactoryErrorReport()->getJobs();
        }
        return $this->daoJobs;
    }

    /**
     * @return \Db\Factory\ErrorReport
     */
    protected function getDaoFactoryErrorReport()
    {
        if ($this->daoFactoryErrorReport === null) {

            $this->daoFactoryErrorReport = $this->getServiceManager()->get('DaoFactoryErrorReport');
        }
        return $this->daoFactoryErrorReport;
    }


    protected function getDb()
    {
        return $this->getServiceManager()->get('error_report_db');
    }


    /**
     * @return \Zend\ServiceManager\Config
     */
    protected function getGlobalConfigs()
    {
        return $this->getServiceManager()->get('globalConfig');
    }

    protected function getJobRawData()
    {
        return $this->jobRawData;
    }


    protected function updateJobStartDate($id)
    {
        $this->getDaoJobs()->update(['last_active_date' => \Helpers\DateTime::create()->now()], ['id' => $id]);
    }

    protected function updateJobElapsedTime($id, $time)
    {
        $this->getDaoJobs()->update(['last_elapsed_time' => $time], ['id' => $id]);
    }

    public function getEncoding()
    {
        return $this->encoding;
    }

    public function setEncoding($encoding)
    {
        $this->encoding = $encoding;
    }

    /**
     * @return null
     */
    public function getIdStatic()
    {
        return $this->idStatic;
    }

    /**
     * @param null $idStatic
     */
    public function setIdStatic($idStatic)
    {

        $idStatic = \Helpers\Strings::create()->clearDigits($idStatic);

        if ($idStatic) {
            $this->idStatic = $idStatic;
        }
    }


}
