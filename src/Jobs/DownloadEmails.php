<?php
/**
 * Created by IntelliJ IDEA.
 * User: namax
 * Date: 26/07/16
 * Time: 07:03 PM
 */

namespace Jobs;

class DownloadEmails extends \Jobs\AbstractJob
{
    protected function configure()
    {
    }

    protected function doAll()
    {
        /* try to connect */
        $inbox = imap_open(
            $this->getSettings()['hostname'],
            $this->getSettings()['username'],
            $this->getSettings()['password']
        ) or die('Cannot connect to Gmail: ' . imap_last_error());

        $totalEmails = imap_num_msg($inbox);

        $this->getLogCli()->info("Total: " . $totalEmails);

        /* grab emails */
        $emails = imap_search($inbox, 'ALL');

        /* if emails are returned, cycle through each... */
        if ($emails) {

            /* begin output var */

            /* put the newest emails on top */
            rsort($emails);

            /* for every email... */
            foreach ($emails as $mail) {
                $output = '';

                $data = [];

                $headerInfo = imap_headerinfo($inbox, $mail);

                // $this->getLogCli()->info( "Subject: " . $headerInfo->subject);
                // $this->getLogCli()->info( "Subject: " . $headerInfo->toaddress);
                // $this->getLogCli()->info( "Subject: " . $headerInfo->date);
                // $this->getLogCli()->info( "Subject: " . $headerInfo->fromaddress);
                // $this->getLogCli()->info( "Subject: " . $headerInfo->reply_toaddress);

                $emailStructure = imap_fetchstructure($inbox, $mail);
                // var_dump($emailStructure);
                if (!isset($emailStructure->parts)) {
                    $this->getLogCli()->warn("Message: ");
                    $output = imap_body($inbox, $mail, FT_PEEK);
                    echo $output . PHP_EOL;
                } else {
                    //
                }

                /* get information specific to this email */
                $overview = imap_fetch_overview($inbox, $mail, 0);
                $message = imap_fetchbody($inbox, $mail, 2);

                /* output the email header information */
                $this->getLogCli()->info("read: " . $overview[0]->seen);
                $this->getLogCli()->info("Subject: " . $overview[0]->subject);
                $this->getLogCli()->info("to: " . $overview[0]->to);
                $this->getLogCli()->info("from: " . $overview[0]->from);
                $this->getLogCli()->info("date: " . $overview[0]->date);

                /* output the email body */
                // echo $message . PHP_EOL;

                $data['from'] = $overview[0]->from;
                $data['to'] = $overview[0]->to;
                $date = new \DateTime($overview[0]->date);
                $data['date'] = \Helpers\DateTime::create()->printDate($date);
                $data['subject'] = $overview[0]->subject;
                $data['body'] = $message ?: $output;
                $this->getDaoFactoryErrorReport()->getRawEmails()->insert($data);
                $this->getLogCli()->info("Saved");
            }

            /* close the connection */
            imap_close($inbox);
        }
    }
}