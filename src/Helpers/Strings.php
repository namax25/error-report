<?php

namespace Helpers;

/**
 * Description of Ads
 * Помощник для работы с объявлениями
 * @author namax
 */
class Strings extends \AbstractHelper
{

    /**
     * Проверка содержит ли строка заданную подстроку
     * @param $substring
     * @param $string
     * @param bool $utf
     * @param bool $caseInsensitive
     * @return bool
     */
    public function isStrContain($substring, $string, $utf = true, $caseInsensitive = true)
    {

        if ($utf) {
            $pos = $caseInsensitive ?
                mb_stripos($string, $substring, 0, "UTF-8") : mb_strpos($string, $substring, 0, "UTF-8");
        } else {
            $pos = $caseInsensitive ? stripos($string, $substring) : strpos($string, $substring);
        }
        return $pos === false ? false : true;


    }


    public function isStrContainAnd(array $substrings, $string, $utf = true, $caseInsensitive = true)
    {
        if (!$substrings) {
            return false;
        }

        foreach ($substrings as $str) {
            if (!$this->isStrContain($str, $string, $utf, $caseInsensitive)) {
                return false;
            }
        }
        return true;
    }


    public function isStrContainOr(array $substrings, $string, $utf = true, $caseInsensitive = true)
    {
        if (!$substrings) {
            return false;
        }

        foreach ($substrings as $str) {
            if ($this->isStrContain($str, $string, $utf, $caseInsensitive)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Убирает все не нужные пробелы
     * @param $string исходная строка
     * @return string строка без пробелов
     */
    public function htmlTrim($string)
    {
        $pattern = '([ \s\t\n\r\x0B\x00\x{A0}\x{AD}\x{2000}-\x{200F}\x{201F}\x{202F}\x{3000}\x{FEFF}]|' .
            '&nbsp;|&#160;|<br\s*\/?>)+';
        return trim(preg_replace('/' . $pattern . '|' . $pattern . '/u', ' ', $string));
    }

    public function clear($string)
    {
        return $this->clearTemplate(
            $string,
            $this->commonReplaceFunc("/[^a-zA-Z0-9а-яА-ЯёЁ\_\=\ \{\}\[\]\(\)\,\.\!\?\@\*\+\-\;\:\|\/\#]+/iu")
        );
    }

    public function clearMd5Hash($string)
    {
        return $this->clearTemplate(
            $string,
            function ($string) {
                $string = mb_strtolower($string);
                return preg_replace("/[^abcdef0-9]+/iu", "", $string);
            }
        );
    }

    public function clearAlias($string)
    {

        return $this->clearTemplate(
            $string,
            function ($string) {
                $string = mb_strtolower($string);
                return preg_replace("/[^a-zA-Z0-9\_]+/iu", "", $string);
            }
        );
    }

    public function clearDigits($string)
    {
        return $this->clearTemplate($string, $this->commonReplaceFunc("/\D/u"));
    }

    public function clearDecimal($string)
    {
        return $this->clearTemplate($string, $this->commonReplaceFunc("/[^0-9\.]+/u"));
    }


    /**
     * Первая буква заглавная
     * @param $str
     * @param bool $utf
     * @return string
     */
    public function ucfirst($str, $utf = true)
    {
        if ($utf) {
            $firstCharacter = mb_strtoupper(mb_substr($str, 0, 1, "UTF-8"), "UTF-8");
            $len = mb_strlen($str) - 1;
            return $firstCharacter . mb_substr($str, 1, $len, "UTF-8");
        } else {
            return ucfirst($str);
        }
    }

    /**
     * Шаблон для работы с массивами и строками
     * @param $string
     * @param $func
     * @return array|bool
     */
    private function clearTemplate($string, $func)
    {
        if (!is_callable($func)) {
            return false;
        }
        if (is_array($string)) {
            array_walk($string, function (&$el) use ($func) {
                $el = $func($el);
            });
            return $string;
        } else {
            return $func($string);
        }
    }

    /**
     * Обшая функция замены
     * @param $pattern
     * @return callable
     */
    private function commonReplaceFunc($pattern)
    {
        return function ($string) use ($pattern) {
            return preg_replace($pattern, "", $string);
        };
    }

}
