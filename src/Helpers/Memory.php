<?php
/**
 * User: namax
 * Date: 23/10/14
 * Time: 05:34 AM
 */

namespace Helpers;


class Memory extends \AbstractHelper
{
    public function show()
    {
        $size = memory_get_usage(true);
        $unit = array('b', 'kb', 'mb', 'gb', 'tb', 'pb');
        return "Memory usage: " . @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
    }

}