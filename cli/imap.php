<?php
/**
 * Created by IntelliJ IDEA.
 * User: namax
 * Date: 25/07/16
 * Time: 03:29 PM
 */

error_reporting(E_ALL);
ini_set('display_errors', '1');
set_time_limit(0);

ini_set('memory_limit', '512M');

defined('ROOT_PATH') || define('ROOT_PATH', realpath(__DIR__ . '/../'));

require_once ROOT_PATH . '/vendor/autoload.php';

$hostname = '{imap.gmail.com:993/imap/ssl}Error summary report';
$username = 'maxim@samauctionsoftware.com';
$jsonStr = file_get_contents(ROOT_DIR . "/config/gmail.local.json");
$jsonCfg = json_decode($jsonStr, true);
$password = $jsonCfg['installed']['pass'];

/* try to connect */
$inbox = imap_open($hostname, $username, $password) or die('Cannot connect to Gmail: ' . imap_last_error());

$tot = imap_num_msg($inbox);

echo "Total: " . $tot . PHP_EOL;

/* grab emails */
$emails = imap_search($inbox, 'ALL');

/* if emails are returned, cycle through each... */
if ($emails) {

    /* begin output var */
    $output = '';

    /* put the newest emails on top */
    rsort($emails);

    /* for every email... */
    foreach ($emails as $mail) {

        $headerInfo = imap_headerinfo($inbox, $mail);

        echo "Subject: " . $headerInfo->subject . PHP_EOL;
        echo "toaddress: " . $headerInfo->toaddress . PHP_EOL;
        echo "date: " . $headerInfo->date . PHP_EOL;
        echo "fromaddress: " . $headerInfo->fromaddress . PHP_EOL;
        echo "reply_toaddress: " . $headerInfo->reply_toaddress . PHP_EOL;

        $emailStructure = imap_fetchstructure($inbox, $mail);

        if (!isset($emailStructure->parts)) {
            echo 'Message' . PHP_EOL;
            $output = imap_body($inbox, $mail, FT_PEEK);
            echo $output . PHP_EOL;
        } else {
            //
        }

        $output = '';
        echo "--------------------------" . PHP_EOL . PHP_EOL;

        /* get information specific to this email */
        $overview = imap_fetch_overview($inbox, $mail, 0);
        $message = imap_fetchbody($inbox, $mail, 2);

        /* output the email header information */
        echo "read: " . $overview[0]->seen . PHP_EOL;
        echo "subject: " . $overview[0]->subject . PHP_EOL;
        echo "from: " . $overview[0]->from . PHP_EOL;
        echo "date: " . $overview[0]->date . PHP_EOL;
        /* output the email body */
        echo $message . PHP_EOL;
        echo "========================================" . PHP_EOL . PHP_EOL;
    }
}

/* close the connection */
imap_close($inbox);