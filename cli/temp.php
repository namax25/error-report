<?php

system("clear");
mb_internal_encoding('UTF-8');
error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('memory_limit', '256M');
set_time_limit(0);

defined('ROOT_PATH') || define('ROOT_PATH', realpath(__DIR__ . '/../'));

//Подключение автозагрузки классов
require_once ROOT_PATH . '/vendor/autoload.php';
// require_once ROOT_PATH . '/vendor/mgargano/simplehtmldom/src/simple_html_dom.php';

$configArray = [
    'driver' => 'Pdo',
    'dsn' => 'mysql:dbname=grabbers;host=localhost',
    'username' => 'root',
    'password' => '1',
    'driver_options' => [
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
    ]
];

//Конфигурация базы
$dbConfigs = new \Zend\Config\Config(require ROOT_PATH . '/cli/config/db/global.php');

//Подключение локального конфига базы
$localConfig = ROOT_PATH . '/cli/config/db/local.php';
if (file_exists($localConfig)) {
    $dbConfigs->merge(new \Zend\Config\Config(require ROOT_PATH . '/cli/config/db/local.php'));
}

$log_cli = prepareLog();

$db = new Zend\Db\Adapter\Adapter($dbConfigs->error_report->toArray());

//Глобальный конфиг
$jobsConfigArr = require ROOT_PATH . '/cli/config/jobs.config.php';


//Конфиг сервис менеджера
$smGlobalConfig = [];
if (isset($jobsConfigArr['service_manager'])) {

    $smGlobalConfig = $jobsConfigArr['service_manager'];
    unset($jobsConfigArr['service_manager']);
}

$globalConfig = new \Zend\Config\Config($jobsConfigArr);

//Подключение локального конфига
$localJobConfig = ROOT_PATH . '/cli/config/jobs.config.local.php';
if (file_exists($localJobConfig)) {
    $globalConfig->merge(new \Zend\Config\Config(require ROOT_PATH . '/cli/config/jobs.config.local.php'));
}

$serviceManager = new \Zend\ServiceManager\ServiceManager();
$serviceManager->setService('error_report_db', $db);
$serviceManager->setService('log_cli', $log_cli);


$serviceManagerConf = new \Zend\Mvc\Service\ServiceManagerConfig($smGlobalConfig);
$serviceManagerConf->configureServiceManager($serviceManager);


//------------------------------------------------------------------------------
$log_cli->warn("Memory used: " . mem());
//------------------------------------------------------------------------------
main();
//------------------------------------------------------------------------------
$log_cli->warn("Memory used: " . mem());

//------------------------------------------------------------------------------


function main()
{
    global $log_cli, $db, $serviceManager;
    $reportTimeStr = "sadf";
    var_dump(new \DateTime($reportTimeStr));



}



//------------------------------------------------------------------------------

function prepareLog()
{
    $processor = new \Log\Processor\Backtrace();
    $formatter_cli = new \Log\Formatter\ColorCli();
    $log_cli = new \Log\Logger();
    $writerStream = new \Zend\Log\Writer\Stream('php://output');
    $writerStream->setFormatter($formatter_cli);
    $log_cli->addProcessor($processor);
    $log_cli->addWriter($writerStream);
    return $log_cli;
}

function mem()
{
    $size = memory_get_usage(true);
    $unit = ['b', 'kb', 'mb', 'gb', 'tb', 'pb'];
    return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
}



