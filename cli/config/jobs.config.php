<?php

return [
    'service_manager' => [
        'factories' => [
            'DaoFactoryErrorReport' => function ($sm) {
                return new \Db\Factory\ErrorReport($sm->get('error_report_db'));
            },
            'log_db' => function ($sm) {
                $log_db = new \Log\Logger();
                $processor = new \Log\Processor\Backtrace();
                $log_db->addProcessor($processor);
                $mapColumn = [
                    'trace' => 'trace',
                    'timestamp' => 'date',
                    'message' => 'message',
                    'priority' => 'priority'
                ];
                $writer = new \Log\Writer\Db($sm->get('error_report_db'), "log", $mapColumn);
                $log_db->addWriter($writer);
                return $log_db;
            }
        ],
    ],
    'cookie_path' => [
        "multi" => function () {
            return realpath(
                ROOT_PATH .
                DIRECTORY_SEPARATOR . 'tmp' .
                DIRECTORY_SEPARATOR . 'cookies'
            );
        }
    ],
    'tesseract_path' => "/usr/local/bin/tesseract"

];
