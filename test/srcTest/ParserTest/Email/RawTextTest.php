<?php
/**
 * Created by IntelliJ IDEA.
 * User: namax
 * Date: 22/08/16
 * Time: 08:26 PM
 */

namespace srcTest\ParserTest\Email;

use Parser\Email\RawText;
use PHPUnit\Framework\TestCase;

class TextWithBadDelimeterTest extends TestCase
{

    public function testParse()
    {

        $text = <<<TEXT
leilocar.auctionserver.net Error summary report 08/22/2016 10:17pm GMT

Find detailed error messages in /var/www/vhosts/auctionserver.net/subdomain=
s/leilocar/httpdocs/logs/error_log


91x: /var/www/vhosts/auctionserver.net/subdomains/leilocar/httpdocs/app/def=
ault/views/drafts/place_bid_confirm_form.php(117) Error - PHP Error - Undef=
ined offset: 0

43x: /var/www/vhosts/auctionserver.net/subdomains/leilocar/httpdocs/app/def=
ault/views/drafts/tell_friend_form.php(78) Error - PHP Error - Creating def=
ault object from empty value

3x: /var/www/vhosts/auctionserver.net/subdomains/leilocar/httpdocs/app/admi=
n/views/drafts/email_template_form.php(391) Error - PHP Error - Creating de=
fault object from empty value

1x: /var/www/vhosts/auctionserver.net/subdomains/leilocar/httpdocs/app/admi=
n/views/scripts/manage-auctions/bid-book.tpl.php(244) Error - PHP Error - U=
ndefined variable: objAucLot





Find the files by=20
$ grep -l "/path/to/file.php(line number)" logs/error_log/*.html

After fixing the issue, please remove the respective files in logs/error_lo=
g br=20
$ for F in logs/error_log/*.html; do grep -l "/path/to/file.php(line number=
)"   | xargs rm -f; done

Report generation time 43.816344976425s=20


TEXT;

        $parser = new RawText();

        $data = [];
        $result = $parser->parse($text, $data);

        $this->assertCount(4, $result);
        $error = $result[0];
        $this->assertEquals("91", $error['count_error_occured']);
        $this->assertEquals("/var/www/vhosts/auctionserver.net/subdomains/leilocar/httpdocs/app/default/views/drafts/place_bid_confirm_form.php", $error['file_path']);
        $this->assertEquals("117", $error['line']);
        $this->assertEquals("Error - PHP Error - Undefined offset: 0", $error['error_text']);
    }

}
