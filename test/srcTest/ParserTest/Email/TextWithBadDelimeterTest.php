<?php
/**
 * Created by IntelliJ IDEA.
 * User: namax
 * Date: 22/08/16
 * Time: 04:45 PM
 */

namespace ParserTest\Email;

use Parser\Email\TextWithBadDelimeter;
use PHPUnit\Framework\TestCase;

class TextWithBadDelimeterTest extends TestCase
{

    public function testParse()
    {

        $text = <<<TEXT
navilleauction.auctionserver.net Error summary report 08/22/2016 10:17pm=
 GMT=0D=0A=0D=0AFind detailed error messages in /var/www/vhosts/auctions=
erver.net/subdomains/navilleauction/httpdocs/logs/error_log=0D=0A=0D=0A=
=0D=0A74x: /var/www/vhosts/auctionserver.net/subdomains/navilleauction/h=
ttpdocs/app/default/views/drafts/auction_lot_live_form.php(1930) Error -=
 PHP Error - urlencode() expects parameter 1 to be string, array given=
=0D=0A=0D=0A35x: /var/www/vhosts/auctionserver.net/subdomains/navilleauc=
tion/httpdocs/includes/classes/Invoice/PdfExportInvoice.class.php(1496)=
 Error - PHP Error - Trying to get property of non-object=0D=0A=0D=0A21x=
: /var/www/vhosts/auctionserver.net/subdomains/navilleauction/httpdocs/i=
ncludes/libs/Zend/Session.php(493) Exception - PHP Exception - Zend_Sess=
ion::start() - /var/www/vhosts/auctionserver.net/subdomains/navilleaucti=
on/httpdocs/includes/libs/Zend/Session.php(Line:480): Error #2 session_s=
tart(): The session id is too long or contains illegal characters, valid=
 characters are a-z, A-Z, 0-9 and '-,' Array=0D=0A=0D=0A1x: /var/www/vhosts/aucti=
onserver.net/subdomains/navilleauction/httpdocs/includes/classes/Qform/U=
ploadHelper.class.php(199) Error - PHP Error - Undefined variable: objLo=
tImg=0D=0A=0D=0A=0D=0A=0D=0A=0D=0A=0D=0AFind the files by =0D=0A$ grep -=
l "/path/to/file.php(line number)" logs/error_log/*.html=0D=0A=0D=0AAfte=
r fixing the issue, please remove the respective files in logs/error_log=
 br =0D=0A$ for F in logs/error_log/*.html; do grep -l "/path/to/file.ph=
p(line number)"   | xargs rm -f; done=0D=0A=0D=0AReport generation time=
 52.135498046875s =0D=0AMemory usage 79.4 MB=0D=0A

TEXT;

        $parser = new TextWithBadDelimeter();

        $data = [];
        $result = $parser->parse($text, $data);

        $this->assertCount(4, $result);
        $error = $result[0];
        $this->assertEquals("74", $error['count_error_occured']);
        $this->assertEquals("/var/www/vhosts/auctionserver.net/subdomains/navilleauction/httpdocs/app/default/views/drafts/auction_lot_live_form.php", $error['file_path']);
        $this->assertEquals("1930", $error['line']);
        $this->assertEquals("Error - PHP Error - urlencode() expects parameter 1 to be string, array given", $error['error_text']);
    }
}