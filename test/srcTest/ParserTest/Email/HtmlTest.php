<?php
/**
 * Created by IntelliJ IDEA.
 * User: namax
 * Date: 22/08/16
 * Time: 09:42 PM
 */

namespace srcTest\ParserTest\Email;

use Parser\Email\Html;
use PHPUnit\Framework\TestCase;

class HtmlTest extends TestCase
{

    public function testParse()
    {

        $text = <<<TEXT
<html><body>
<p>auctions.bidsquare.com (108.61.6.67) Error summary report 08/22/2016 11:=
05pm GMT</p>
<p>Find detailed error messages in /var/www/vhosts/auctionserver.net/subdom=
ains/iap/httpdocs/logs/error_log</p>
<p>6x: /var/www/vhosts/auctionserver.net/subdomains/iap/httpdocs/wwwroot/lo=
cation/index.php(14) Error &ndash; PHP Error &ndash; readfile(/var/www/vhos=
ts/auctionserver.net/subdomains/iap/httpdocs/wwwroot/images/settings/LL_122=
_145.jpg): failed to open stream: No such file or directory</p>
<p>Find the files by $ grep -l &ldquo;/path/to/file.php(line number)&rdquo;=
 logs/error_log/*.html</p>
<p>After fixing the issue, please remove the respective files in logs/error=
_log br $ for F in logs/error_log/*.html; do grep -l &ldquo;/path/to/file.p=
hp(line number)&rdquo;   | xargs rm -f; done</p>
<p>Report generation time 0.59133005142212s Memory usage 2.62 MB</p>

<img src=3D"https://u2269598.ct.sendgrid.net/wf/open?upn=3DEjbh2CBIt-2BjmRV=
BC7J59R7SqjOju-2FkPv-2F6UBZx7wliDnQeXbYduraksGgpk0hb2b5rphoC0J16duv3D8YFieR=
pNAKPG1NENkCquadb8ET4UiHDR8IVFRKPxZ3qaP1MlxKDZ9qq6wKlPtk7kMZaDXfrYzXK3cCBVf=
EBEuCK5psIPwpJDqiiMp0vK1ULEWojdNkuofsM8vwBMwKvrbDU35HYcTLnm1iMMNqRIFDs92wg8=
-3D" alt=3D"" width=3D"1" height=3D"1" border=3D"0" style=3D"height:1px !im=
portant;width:1px !important;border-width:0 !important;margin-top:0 !import=
ant;margin-bottom:0 !important;margin-right:0 !important;margin-left:0 !imp=
ortant;padding-top:0 !important;padding-bottom:0 !important;padding-right:0=
 !important;padding-left:0 !important;"/>
</body></html>
TEXT;

        $parser = new Html();

        $data = [];
        $result = $parser->parse($text, $data);

        $this->assertCount(1, $result);
        $error = $result[0];
        $this->assertEquals("6", $error['count_error_occured']);
        $this->assertEquals("/var/www/vhosts/auctionserver.net/subdomains/iap/httpdocs/wwwroot/location/index.php", $error['file_path']);
        $this->assertEquals("14", $error['line']);
        $this->assertEquals("Error - PHP Error - readfile(/var/www/vhosts/auctionserver.net/subdomains/iap/httpdocs/wwwroot/images/settings/LL_122_145.jpg): failed to open stream: No such file or directory", $error['error_text']);
    }

}
